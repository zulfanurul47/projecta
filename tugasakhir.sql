-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2022 at 02:44 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugasakhir`
--

-- --------------------------------------------------------

--
-- Table structure for table `carousel`
--

CREATE TABLE `carousel` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `foto_carousel` varchar(255) NOT NULL,
  `status` enum('Disetujui','Belum Disetujui','Tidak Disetujui') NOT NULL DEFAULT 'Belum Disetujui'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carousel`
--

INSERT INTO `carousel` (`id`, `title`, `description`, `foto_carousel`, `status`) VALUES
(8, 'Carousel 1', 'Ini Carousel 1', 'Produk6.jpg', 'Disetujui'),
(9, 'Carousel 2', 'Ini Carousel 2', 'Produk7.jpg', 'Disetujui'),
(10, 'Carousel 3', 'Ini Carousel 3', 'Produk111.jpg', 'Disetujui');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `jenis_barang` varchar(255) NOT NULL,
  `harga_barang` int(11) NOT NULL,
  `foto_barang` varchar(255) NOT NULL,
  `status` enum('Disetujui','Belum Disetujui','Tidak Disetujui') NOT NULL DEFAULT 'Belum Disetujui'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `nama_barang`, `jenis_barang`, `harga_barang`, `foto_barang`, `status`) VALUES
(1, 'Kursi Bahan Premium', 'Kursi', 200000, 'Produk8.jpg', 'Tidak Disetujui'),
(2, 'Sofa Empuk Premium Anti Karat', 'Sofa', 1000000, 'Produk6.jpg', 'Disetujui'),
(3, 'Meja Belajar Laptop Serbaguna', 'Meja', 250000, 'Produk3.jpg', 'Belum Disetujui'),
(4, 'Set Kursi Santa', 'Kursi', 1000000, 'Produk9.jpg', 'Disetujui'),
(5, 'Set Lemari Kamar', 'Lemari', 1100000, 'Produk10.jpg', 'Disetujui'),
(6, 'Set Meja Kursi Ruang Keluarga Premium', 'Meja Kursi', 1500000, 'Produk111.jpg', 'Disetujui'),
(7, 'Set Meja Kursi Ruang Keluarga Super Premium', 'Meja Kursi', 2500000, 'Produk12.jpg', 'Disetujui');

-- --------------------------------------------------------

--
-- Table structure for table `table_user`
--

CREATE TABLE `table_user` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `table_user`
--

INSERT INTO `table_user` (`id`, `nama_lengkap`, `username`, `password`, `level`) VALUES
(1, 'Zulfa Nurul Ikhsan', 'staff', 'staff', 'Staff'),
(2, 'H. Zulfa Nurul Ikhsan, S.Kom.', 'manager', 'manager', 'Manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_user`
--
ALTER TABLE `table_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carousel`
--
ALTER TABLE `carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `table_user`
--
ALTER TABLE `table_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
