<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_product extends CI_Model{
    public function getdata(){
        //$this->db->order_by('id', 'DESC');
        $data = $this->db->get('product')->result();
        return $data;
    }

    public function edit($id){
        $this->db->where('id',$id);
        $data = $this->db->get('product')->row();
        return $data;
    }

    public function edit_save($data,$id){
        return $this->db->update('product', $data, ['id' => $id]);
        /* $this->db->where('id',$id);
        $update = $this->db->update('product',$data);
        return $update; */
    }

    public function checkImage($id){
        $query = $this->db->get_where('product', ['id' => $id]);
        return $query->row();
    }

    public function deletePro($id){
        return $this->db->delete('product', ['id' => $id]);
    }

    function getaccproduct(){
        $query = $this->db->get_where('product', ['status' => 'Disetujui'])->result();
        return $query;
    }

    function filtering($nama_barang,$jenis_barang,$harga_minimal=null,$harga_maksimal=null){
        $this->db->select('*');
        $this->db->where('status', 'Disetujui');
        if($nama_barang != null){
            $this->db->like('nama_barang', $nama_barang);
        }
        if($jenis_barang != null){
            $this->db->like('jenis_barang', $jenis_barang);
        }
        if($harga_minimal != null && $harga_maksimal != null){
            $this->db->where('harga_barang >=', $harga_minimal);
            $this->db->where('harga_barang <=', $harga_maksimal);
        }
        return $this->db->get('product')->result();
    }
    
    function filters(){
        $res_nama = $this->db->like('nama_barang', '%');
    }

    /* public function delete($id){
        $this->db->where('id', $id);
        $delete = $this->db->delete('product');
        return $delete;
    } */
}

?>