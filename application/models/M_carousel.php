<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_carousel extends CI_Model{
    function __construct() {
         parent::__construct();
        $this->load->database();
      }

    public function getdata(){
        //$this->db->order_by('id', 'DESC');
        $data = $this->db->get('carousel')->result();
        return $data;
    }

    function getacccarousel(){
        $query = $this->db->query("SELECT * FROM carousel where status = 'Disetujui'");
        return $query->result_array();
    }

    public function edit($id){
        $this->db->where('id',$id);
        $data = $this->db->get('carousel')->row();
        return $data;
    }

    public function edit_save($data,$id){
        return $this->db->update('carousel', $data, ['id' => $id]);
        /* $this->db->where('id',$id);
        $update = $this->db->update('carousel',$data);
        return $update; */
    }

    public function checkImage($id){
        $query = $this->db->get_where('carousel', ['id' => $id]);
        return $query->row();
    }

    public function deletePro($id){
        return $this->db->delete('carousel', ['id' => $id]);
    }

    /* public function delete($id){
        $this->db->where('id', $id);
        $delete = $this->db->delete('carousel');
        return $delete;
    } */
}

?>