<?php if ($this->session->flashdata('status')) { ?>
    <div class="alert alert-success"> <?= $this->session->flashdata('status') ?> </div>
<?php } else if ($this->session->flashdata('warning')) { ?>
    <div class="alert alert-danger"> <?= $this->session->flashdata('warning') ?> </div>
<?php } ?>

<section class="section">
    <div class="row">
        <form action="<?= site_url('administrator/carousel/edit_save') ?>" method="POST" enctype="multipart/form-data">
            <input type="hidden" id="id" name="id" value="<?= $carousel->id ?>">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Title</label>
                    <input id="title" name="title" type="text" class="form-control" value="<?= $carousel->title ?>" aria-describedby="emailHelp" required>
                    <small><?= form_error('title') ?></small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    <input id="description" name="description" type="text" class="form-control" value="<?= $carousel->description ?>" required>
                    <small><?= form_error('description') ?></small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Foto</label>
                    <div class="col-md-4">
                        <img src="<?= base_url('/uploads/carousel/' . $carousel->foto_carousel) ?>" alt="" class="w-100">
                    </div>
                    <input type="hidden" name="old_foto_carousel" value="<?= $carousel->foto_carousel ?>">
                    <input id="foto_carousel" name="foto_carousel" type="file" class="form-control" value="<?= $carousel->foto_carousel ?>">
                </div>
                <?php if($this->session->userdata('username')!='staff'){ ?>
                    <div class="form-group">
                    <label for="exampleInputPassword1">Status</label>
                    <div class="input-group form-control">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">Options</label>
                        </div>
                        <select class="custom-select" id="statusSelect" name="status">
                            <option value="Disetujui">Disetujui</option>
                            <option value="Belum Disetujui" selected>Belum Disetujui</option>
                            <option value="Tidak Disetujui">Tidak Disetujui</option>
                        </select>
                    </div>
                    <small><?= form_error('status') ?></small>
                </div>
                    <?php } ?>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
    </div>
</section>