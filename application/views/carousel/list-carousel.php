<section class="section dashboard">
    <div class="row">
        <div class="col-lg-6">

            <div class="card" style="width: 900px;">
                <div class="card-body">
                    <h5 class="card-title">Carousel Table</h5>
                    <?php if ($this->session->flashdata('status')) { ?>
                        <div class="alert alert-success"> <?= $this->session->flashdata('status') ?> </div>
                    <?php } ?>

                    <?php if ($this->session->userdata('username') != 'manager') { ?>
                        <a href="<?= site_url('administrator/carousel/add') ?>" type="button" class="btn btn-primary">Tambah Data</a>
                    <?php } ?>

                    <!-- Default Table -->
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Description</th>
                                <th scope="col">Foto</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            ?>
                            <?php foreach ($carousel as $row) { ?>
                                <tr>
                                    <th scope="row"><?= $no; ?></th>
                                    <td><?= $row->title; ?></td>
                                    <td><?= $row->description; ?></td>
                                    <td><img src="<?= base_url('/uploads/carousel/' . $row->foto_carousel) ?>" alt="" style="width: 100px;"></td>
                                    <td><?= $row->status; ?></td>
                                    <td>
                                        <a href="<?= site_url() ?>/administrator/carousel/edit/<?= $row->id ?>" type="button" class="btn btn-outline-warning">Edit</a>
                                        <?php if ($this->session->userdata('username') != 'staff') { ?>
                                            <a href="<?= site_url() ?>/administrator/carousel/hapus/<?= $row->id ?>" type="button" class="btn btn-outline-danger">Hapus</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- End Default Table Example -->
                </div>
            </div>
        </div>
</section>