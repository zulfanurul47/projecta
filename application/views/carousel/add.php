<?php if ($this->session->flashdata('status')) { ?>
  <div class="alert alert-success"> <?= $this->session->flashdata('status') ?> </div>
<?php } else if ($this->session->flashdata('warning')) { ?>
  <div class="alert alert-danger"> <?= $this->session->flashdata('warning') ?> </div>
<?php } ?>
<section class="section">
  <div class="row">

    <form action="<?= site_url('administrator/carousel/tambah') ?>" method="POST" enctype="multipart/form-data">
      <div class="form-group">
        <label for="exampleInputEmail1">Title</label>
        <input id="title" name="title" type="text" class="form-control" aria-describedby="emailHelp" required>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Description</label>
        <input id="description" name="description" type="textarea" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Foto</label>
        <input id="foto_carousel" name="foto_carousel" type="file" class="form-control" required>
      </div>
      <button type="submit" class="btn btn-primary">Simpan</button>
    </form>

  </div>
</section>