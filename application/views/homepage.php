<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
   <link rel="stylesheet" href="<?= base_url()?>assets/css/style.css">
   <link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.min.css">
   <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
   <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url()?>assets/img/Logo.png">
   <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url()?>assets/img/Logo.png">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
   <title>Zulfa Furniture</title>
</head>

<body>
   <nav class="navbar navbar-expand-lg bg-light">
      <div class="container">
         <a class="navbar-brand" href="">
            Zulfa Furniture
         </a>
         <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
               <li class="nav-item ">
                  <a class="nav-link menu" id="home" href="<?= site_url('Homepage/index')?>">Home</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link menu" id="product" href="<?= site_url('Homepage/product')?>">Product</a>
               </li>
               <li class="nav-item">
                  <a type="button" class="btn btn-primary" id="login" href="<?= site_url('Homepage/login')?>">Log In</a>
               </li>
         </div>
      </div>
   </nav>

   <!-- Banner Section -->
   <header class="bg-dark py-5">
      <div class="container px-4 px-lg-5 my-5">
         <div class="text-center text-white">
            <h1 class="display-4 fw-bolder" style="font-family: Helvetica;">Zulfa Furniture</h1>
            <p class="lead fw-normal text-white-50 mb-0">Memenuhi Kebutuhan Furniture Rumah Anda</p>
         </div>
      </div>
   </header>

   <!-- Carousel Section -->
   <section class="caro" id="caro">
      <h1 class="sec-head" id="caro-head">Produk Kami</h1>
   </section>
   <div class="container">
      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
         <ol class="carousel-indicators">
            <?= $indicators ?>
         </ol>
         <div class="carousel-inner">
            <?= $slides ?>
         </div>
         <button class="carousel-control-prev" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
         </button>
         <button class="carousel-control-next" type="button" data-target="#carouselExampleCaptions" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
         </button>
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
         <span class="glyphicon glyphicon-chevron-left"></span>
         <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
         <span class="glyphicon glyphicon-chevron-right"></span>
         <span class="sr-only">Next</span>
      </a>
   </div>
   </div>
   </div>
<br><br><br><br><br>
<!-- About Section -->
<section class="page-section" id="services">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Mengapa Harus Kami</h2> <br><br>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-thumbs-up fa-stack-1x fa-inverse"></i>                        
                        </span>
                        <h4 class="my-3">Terpercaya</h4>
                        <p class="text-muted">Kami telah menjadi kepercayaan berbagai kalangan, mulai dari perusahaan, lembaga negara, perumahan, dan perorangan.</p>
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-users fa-stack-1x fa-inverse"></i>                        
                        </span>
                        <h4 class="my-3">Gratis Konsultasi</h4>
                        <p class="text-muted">Konsultasikan bentuk dan konsep hunian anda agar mendapatkan furniture yang cocok dan sempurna.</p>
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-money fa-stack-1x fa-inverse"></i>                        
                        </span>
                        <h4 class="my-3">Harga Terjangkau</h4>
                        <p class="text-muted">Produk kami memiliki harga yang sangat terjangkau dan kami selalu memberikan penawaran potongan harga pada event tertentu.</p>
                    </div>
                </div>
            </div>
        </section>
   <!-- Footer Section -->
   <div class="footer-dark" style="font-family: Inter;">
      <footer>
         <div class="container">
            <div class="row">
               <div class="col item">
                  <h3>Pages</h3>
                  <ul>
                     <li><a href="index.php">Home</a></li>
                     <li><a href="product.php">Product</a></li>
                  </ul>
               </div>
               <div class="col item">
                  <h3>Other Details</h3>
                  <ul>
                     <li><a href="#">Privacy Policy</a></li>
                     <li><a href="#">Terms of Service</a></li>
                  </ul>
               </div>
               <div class="col item text">
                  <h3>Zulfa Furniture</h3>
                  <p>Distributor furniture terbesar di Jakarta Utara. Menjual berbagai macam furniture berkelas lokal, nasional, hingga internasional</p>
               </div>
            </div>
            <div class="social_links">
               <a href="https://www.instagram.com/">
                  <span class="fa-stack fa-lg ig combo">
                     <i class="fa fa-circle fa-stack-2x circle"></i>
                     <i class="fa fa-instagram fa-stack-1x fa-inverse icon"></i>
                  </span>
               </a>
               <a href="https://www.facebook.com/">
                  <span class="fa-stack fa-lg fb combo">
                     <i class="fa fa-circle fa-stack-2x circle"></i>
                     <i class="fa fa-facebook fa-stack-1x fa-inverse icon"></i>
                  </span>
               </a>
               <a href="https://www.youtube.com/">
                  <span class="fa-stack fa-lg yt combo">
                     <i class="fa fa-circle fa-stack-2x circle"></i>
                     <i class="fa fa-youtube-play fa-stack-1x fa-inverse icon"></i>
                  </span>
               </a>
               <a href="https://www.twitter.com/">
                  <span class="fa-stack fa-lg tw combo">
                     <i class="fa fa-circle fa-stack-2x circle"></i>
                     <i class="fa fa-twitter fa-stack-1x fa-inverse icon"></i>
                  </span>
               </a>
               <a href="https://codepen.io">
                  <span class="fa-stack fa-lg gt combo">
                     <i class="fa fa-circle fa-stack-2x circle"></i>
                     <i class="fa fa-codepen fa-stack-1x fa-inverse icon"></i>
                  </span>
               </a>
               <a href="https://www.linkedin.com/">
                  <span class="fa-stack fa-lg tw combo">
                     <i class="fa fa-circle fa-stack-2x circle"></i>
                     <i class="fa fa-linkedin fa-stack-1x fa-inverse icon"></i>
                  </span>
               </a>
            </div>
            <p class="copyright">Zulfa Furniture © 2022</p>
         </div>
      </footer>
   </div>

   <!-- JS -->
   <script src="<?= base_url()?>ts/js/main.js"></script>
   <script src="<?= base_url()?>assets/js/mains.js"></script>
   <script src="<?= base_url()?>assets/js/bootstrap.js"></script>
</body>

</html>