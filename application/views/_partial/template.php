<?php $this->load->view('_partial/head') ?>



<body>
<?php $this->load->view('_partial/header') ?>

<?php $this->load->view('_partial/sidebar') ?>

  <main id="main" class="main">

    <div class="pagetitle">
      <h1><?= $title ?></h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item active"><?= $title ?></li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <?= $body; ?>

  </main><!-- End #main -->

  <?php $this->load->view('_partial/footer') ?>