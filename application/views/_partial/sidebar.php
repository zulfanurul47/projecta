<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

<ul class="sidebar-nav" id="sidebar-nav">

    <li class="nav-item">
        <a class="nav-link " href="<?= site_url('administrator/dashboard') ?>">
            <i class="bi bi-grid"></i>
            <span>Dashboard</span>
        </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
        <a class="nav-link " href="<?= site_url('administrator/carousel') ?>">
            <i class="bi bi-grid"></i>
            <span>Carousel</span>
        </a>
    </li><!-- End Dashboard Nav -->

    <li class="nav-item">
        <a class="nav-link " href="<?= site_url('administrator/product') ?>">
            <i class="bi bi-grid"></i>
            <span>Product</span>
        </a>
    </li><!-- End Dashboard Nav -->



</ul>

</aside><!-- End Sidebar-->