<?php if ($this->session->flashdata('status')) { ?>
  <div class="alert alert-success"> <?= $this->session->flashdata('status') ?> </div>
<?php } else if ($this->session->flashdata('warning')) { ?>
  <div class="alert alert-danger"> <?= $this->session->flashdata('warning') ?> </div>
<?php } ?>
<section class="section">
  <div class="row">

    <form action="<?= site_url('administrator/product/tambah') ?>" method="POST" enctype="multipart/form-data">
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Barang</label>
        <input id="nama_barang" name="nama_barang" type="text" class="form-control" aria-describedby="emailHelp" required>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Jenis Barang</label>
        <input id="jenis_barang" name="jenis_barang" type="textarea" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Harga Barang</label>
        <input id="harga_barang" name="harga_barang" type="textarea" class="form-control" required>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Foto</label>
        <input id="foto_barang" name="foto_barang" type="file" class="form-control" required>
      </div>
      <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
  </div>
</section>