<section class="section dashboard">
    <div class="row">
        <div class="col-lg-6">

            <div class="card" style="width: 900px;">
                <div class="card-body">
                    <h5 class="card-title">Product Table</h5>
                    <?php if ($this->session->flashdata('status')) { ?>
                        <div class="alert alert-success"> <?= $this->session->flashdata('status') ?> </div>
                    <?php } ?>
                    <?php if($this->session->userdata('username')!='manager'){ ?>
                        <a href="<?= site_url('administrator/product/add') ?>" type="button" class="btn btn-primary">Tambah Data</a>
                    <?php } ?>
                    <!-- Default Table -->
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Barang</th>
                                <th scope="col">Jenis Barang</th>
                                <th scope="col">Harga Barang</th>
                                <th scope="col">Foto</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            ?>
                            <?php foreach ($product as $row) { ?>
                                <tr>
                                    <th scope="row"><?= $no; ?></th>
                                    <td><?= $row->nama_barang; ?></td>
                                    <td><?= $row->jenis_barang; ?></td>
                                    <td><?= $row->harga_barang; ?></td>
                                    <td><img src="<?= base_url('/uploads/product/' . $row->foto_barang) ?>" alt="" style="width: 100px;"></td>
                                    <td><?= $row->status; ?></td>
                                    <td>
                                        <a href="<?= site_url() ?>/administrator/product/edit/<?= $row->id ?>" type="button" class="btn btn-outline-warning">Edit</a>
                                        <?php if ($this->session->userdata('username') != 'staff') { ?>
                                            <a href="<?= site_url() ?>/administrator/product/hapus/<?= $row->id ?>" type="button" class="btn btn-outline-danger">Hapus</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- End Default Table Example -->
                </div>
            </div>
        </div>
</section>