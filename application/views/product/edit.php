<?php if ($this->session->flashdata('status')) { ?>
    <div class="alert alert-success"> <?= $this->session->flashdata('status') ?> </div>
<?php } else if ($this->session->flashdata('warning')) { ?>
    <div class="alert alert-danger"> <?= $this->session->flashdata('warning') ?> </div>
<?php } ?>

<section class="section">
    <div class="row">
        <form action="<?= site_url('administrator/product/edit_save') ?>" method="POST" enctype="multipart/form-data">
            <input type="hidden" id="id" name="id" value="<?= $product->id ?>">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Barang</label>
                    <input id="nama_barang" name="nama_barang" type="text" class="form-control" value="<?= $product->nama_barang ?>" aria-describedby="emailHelp" required>
                    <small><?= form_error('nama_barang') ?></small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Jenis Barang</label>
                    <input id="jenis_barang" name="jenis_barang" type="text" class="form-control" value="<?= $product->jenis_barang ?>" required>
                    <small><?= form_error('jenis_barang') ?></small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Harga Barang</label>
                    <input id="harga_barang" name="harga_barang" type="text" class="form-control" value="<?= $product->harga_barang ?>" required>
                    <small><?= form_error('harga_barang') ?></small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Foto</label>
                    <div class="col-md-4">
                        <img src="<?= base_url('/uploads/product/' . $product->foto_barang) ?>" alt="" class="w-100">
                    </div>
                    <input type="hidden" name="old_foto_barang" value="<?= $product->foto_barang ?>">
                    <input id="foto_barang" name="foto_barang" type="file" class="form-control" value="<?= $product->foto_barang ?>">
                    <?php if ($this->session->userdata('username') != 'staff') { ?>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Status</label>
                            <div class="input-group form-control">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">Options</label>
                                </div>
                                <select class="custom-select" id="statusSelect" name="status">
                                    <option value="Disetujui">Disetujui</option>
                                    <option value="Belum Disetujui" selected>Belum Disetujui</option>
                                    <option value="Tidak Disetujui">Tidak Disetujui</option>
                                </select>
                            </div>
                            <small><?= form_error('status') ?></small>
                        </div>
                    <?php } ?>
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
    </div>
</section>