<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>assets/img/Logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>assets/img/Logo.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <title>Zulfa Furniture</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container">
            <a class="navbar-brand" href="#index.php">
                Zulfa Furniture
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mb-2 mb-lg-0">
                    <li class="nav-item ">
                        <a class="nav-link menu" id="home" href="<?= site_url('Homepage/index') ?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link menu" id="product" href="<?= site_url('Homepage/product') ?>">Product</a>
                    </li>
                    <li class="nav-item">
                        <a type="button" class="btn btn-primary" id="login" href="<?= site_url('Homepage/login') ?>">Log In</a>
                    </li>
            </div>
        </div>
    </nav>

    <!-- Banner Section -->
    <header class="bg-dark py-5">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-center text-white">
                <h1 class="display-4 fw-bolder" style="font-family: Helvetica;">Zulfa Furniture</h1>
                <p class="lead fw-normal text-white-50 mb-0">Memenuhi Kebutuhan Furniture Rumah Anda</p>
            </div>
        </div>
    </header>
    <br><br>
    <!-- Filter Section -->
    <section class="container border border-secondary">
        <center>
            <h1 class="text-align-center"><b>Filter</b></h1>
        </center>
        <form action="<?= site_url('homepage/filter') ?>" method="POST">
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">
                    Nama Produk
                </label>
                <div class="col-sm-10">
                    <input type="text" name="nama_barang" class="form-control" id="" value="<?= set_value('nama_barang') ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Jenis Produk</label>
                <div class="col-sm-10">
                    <input type="text" name="jenis_barang" class="form-control" id="" value="<?= set_value('jenis_barang') ?>">
                </div>
            </div>
            <span>Rentang Harga:</span>
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Harga Minimal</label>
                <div class="col-sm-10">
                    <input type="text" name="harga_minimal" class="form-control" id="" value="<?= set_value('harga_minimal') ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label">Harga Maksimal</label>
                <div class="col-sm-10">
                    <input type="text" name="harga_maksimal" class="form-control" id="" value="<?= set_value('harga_maksimal') ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-primary mb-1 w-100">Temukan</button>
            <a href="<?= site_url('homepage/product') ?>" class="btn btn-success mb-1 w-100">Refresh</a>
        </form>
    </section>
    <br>


    <!-- Product Section -->
    <section style="background-color: #eee;">
        <div class="container py-5">
            <center>
                <h1>Produk Kami</h1>
            </center>
            <div class="row">
                <?php foreach ($product as $row) { ?>

                    <div class="col-md-12 col-lg-4 mb-4 mb-lg-0 d-flex">
                        <div class="card">
                            <div class="d-flex justify-content-between p-3">
                                <p class="lead mb-0"> Terbaik!!</p>
                            </div>
                            <img src="<?= base_url('/uploads/product/' . $row->foto_barang); ?>" class="card-img-top" alt="" />
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <p class="small"><a href="#!" class="text-muted">Kategori: <?= $row->jenis_barang; ?></a></p>
                                </div>
                                <div class="d-flex justify-content-between mb-3">
                                    <h5 class="mb-0"><?= $row->nama_barang; ?></h5>
                                    <h5 class="text-dark mb-0">Rp <?= $row->harga_barang; ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if(!$product){ ?>
                    <div class="row alert alert-danger w-100">Produk tidak ditemukan, silahkan cari produk yang lain.</div>
                <?php } ?>
            </div>
        </div>
    </section>

    <!-- Footer Section -->
    <div class="footer-dark" style="font-family: Inter;">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col item">
                        <h3>Pages</h3>
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="product.php">Product</a></li>
                        </ul>
                    </div>
                    <div class="col item">
                        <h3>Other Details</h3>
                        <ul>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms of Service</a></li>
                        </ul>
                    </div>
                    <div class="col item text">
                        <h3>Zulfa Furniture</h3>
                        <p>Distributor furniture terbesar di Jakarta Utara. Menjual berbagai macam furniture berkelas lokal, nasional, hingga internasional</p>
                    </div>
                </div>
                <div class="social_links">
                    <a href="https://www.instagram.com/">
                        <span class="fa-stack fa-lg ig combo">
                            <i class="fa fa-circle fa-stack-2x circle"></i>
                            <i class="fa fa-instagram fa-stack-1x fa-inverse icon"></i>
                        </span>
                    </a>
                    <a href="https://www.facebook.com/">
                        <span class="fa-stack fa-lg fb combo">
                            <i class="fa fa-circle fa-stack-2x circle"></i>
                            <i class="fa fa-facebook fa-stack-1x fa-inverse icon"></i>
                        </span>
                    </a>
                    <a href="https://www.youtube.com/">
                        <span class="fa-stack fa-lg yt combo">
                            <i class="fa fa-circle fa-stack-2x circle"></i>
                            <i class="fa fa-youtube-play fa-stack-1x fa-inverse icon"></i>
                        </span>
                    </a>
                    <a href="https://www.twitter.com/">
                        <span class="fa-stack fa-lg tw combo">
                            <i class="fa fa-circle fa-stack-2x circle"></i>
                            <i class="fa fa-twitter fa-stack-1x fa-inverse icon"></i>
                        </span>
                    </a>
                    <a href="https://codepen.io">
                        <span class="fa-stack fa-lg gt combo">
                            <i class="fa fa-circle fa-stack-2x circle"></i>
                            <i class="fa fa-codepen fa-stack-1x fa-inverse icon"></i>
                        </span>
                    </a>
                    <a href="https://www.linkedin.com/">
                        <span class="fa-stack fa-lg tw combo">
                            <i class="fa fa-circle fa-stack-2x circle"></i>
                            <i class="fa fa-linkedin fa-stack-1x fa-inverse icon"></i>
                        </span>
                    </a>
                </div>
                <p class="copyright">Zulfa Furniture © 2022</p>
            </div>
        </footer>
    </div>

    <!-- JS -->
    <script src="<?= base_url() ?>ts/js/main.js"></script>
    <script src="<?= base_url() ?>assets/js/mains.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
</body>

</html>