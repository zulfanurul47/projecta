<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_auth');
    }
    
    public function cekproses(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $result = $this->m_auth->cek_login($username, $password);
        $numrow = $result->num_rows();
        if($numrow>0){
            $data = $result->row();
            $data_array = array(
                'username' => $data->username,
                'nama_lengkap' => $data->nama_lengkap,
                'level' => $data->level
            );
            $this->session->set_userdata($data_array);
            redirect('administrator/dashboard');
            
            
        }else{
            $this->session->set_flashdata('warning','Username atau Password tidak valid');
            redirect('login');
        }
    }

    public function view(){
        echo "<pre>";
        print_r($this->session->userdata());
        echo "<pre>";
    }

    public function destroy(){
        $this->session->sess_destroy();

        redirect(base_url());
    }
}
