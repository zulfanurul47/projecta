<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_product');
    }


    public function index()
    {
        $this->data['product'] = $this->m_product->getdata();
        $this->data['title'] = 'Product';
        $this->template->load('product/list-product', $this->data);
    }

    public function add()
    {
        $this->data['title'] = "Tambah Product";
        $this->template->load('product/add', $this->data);
    }

    public function tambah()
    {
        $config['upload_path']          = './uploads/product/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 4000;
        $config['max_width']            = 6000;
        $config['max_height']           = 4000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('foto_barang')) {
            $error = array('error' => $this->upload->display_errors());
            $errorstr = implode(" ", $error);

            $this->session->set_flashdata('warning', $errorstr);
            redirect(base_url('index.php/administrator/product/add'));
        } else {
            $data = $this->upload->data();

            $input = $this->input->post();

            $foto['nama_barang'] = $input['nama_barang'];
            $foto['jenis_barang'] = $input['jenis_barang'];
            $foto['harga_barang'] = $input['harga_barang'];
            $foto['foto_barang'] = $data['file_name'];
            $foto['status'] = 'Belum Disetujui';

            $this->db->insert('product', $foto);

            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('status', 'Data Berhasil Disimpan');
                redirect(base_url('index.php/administrator/product/add'));
            } else {
                $this->session->set_flashdata('warning', 'Data Gagal Disimpan');
                redirect(base_url('index.php/administrator/product/add'));
            }
        }
    }

    public function add_save()
    {
        $data = array(
            'nama_barang' => $this->input->post('nama_barang'),
            'jenis_barang' => $this->input->post('jenis_barang'),
            'harga_barang' => $this->input->post('harga_barang'),
            'foto_barang' => $this->input->post('foto_barang'),
            'status' => $this->input->post('status'),
        );

        $simpan = $this->db->insert('product', $data);

        if ($simpan) {
            redirect('administrator/product');
        } else {
            echo "Data Belum Masuk";
        }
    }

    public function edit($id)
    {
        $this->data['product'] = $this->m_product->edit($id);
        $this->data['title'] = 'Edit Product';
        $this->template->load('product/edit', $this->data);
    }

    public function edit_save()
    {
        $id = $this->input->post('id');
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
        $this->form_validation->set_rules('jenis_barang', 'Jenis Barang', 'required');
        $this->form_validation->set_rules('harga_barang', 'Harga Barang', 'required');

        if ($this->form_validation->run()) {
            $old_filename = $this->input->post('old_foto_barang');
            $new_filename = $_FILES['foto_barang']['name'];

            if ($new_filename == TRUE) {
                $update_filename = time() . "-" . str_replace(" ", "-", $_FILES['foto_barang']['name']);

                $config['upload_path']          = './uploads/product/';
                $config['allowed_types']        = 'jpeg|jpg|png';
                $config['max_size']             = 4000;
                $config['max_width']            = 6000;
                $config['max_height']           = 4000;
                $config['file_name']            = $update_filename;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('foto_barang')) {
                    if (file_exists("./uploads/product/" . $old_filename)) {
                        unlink("./uploads/product/" . $old_filename);
                    }
                }
            } else 
            {
                $update_filename = $old_filename;
            }
            $input = $this->input->post();

            $data['nama_barang'] = $input['nama_barang'];
            $data['jenis_barang'] = $input['jenis_barang'];
            $data['harga_barang'] = $input['harga_barang'];
            $data['foto_barang'] = $update_filename;
            $data['status'] = $input['status'];

            $update = new M_product;
            $result = $update->edit_save($data,$id);
            
            $this->session->set_flashdata('status', 'Data Berhasil Diupdate');
                redirect(base_url('index.php/administrator/product/edit/'.$id));
        } else {
            $this->session->set_flashdata('warning', 'Data Gagal Diupdate');
                redirect(base_url('index.php/administrator/product/edit/'.$id));
        }
    }

    public function hapus($id)
    {
        $pro = new M_product;
        if($pro->checkImage($id)){
            $data = $pro->checkImage($id);
            if(file_exists("./uploads/product/" . $data->foto_barang)){
                unlink("./uploads/product/" . $data->foto_barang);
            }
            $pro->deletePro($id);
            $this->session->set_flashdata('status', 'Data Berhasil Dihapus');
            redirect(base_url('index.php/administrator/product'));
        }
    }
}
