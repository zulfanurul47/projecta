<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Carousel extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_carousel');
    }


    public function index()
    {
        $this->data['carousel'] = $this->m_carousel->getdata();
        $this->data['title'] = 'Carousel';
        $this->template->load('carousel/list-carousel', $this->data);
    }

    public function add()
    {
        $this->data['title'] = "Tambah Carousel";
        $this->template->load('carousel/add', $this->data);
    }

    public function tambah()
    {
        $config['upload_path']          = './uploads/carousel/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 4000;
        $config['max_width']            = 6000;
        $config['max_height']           = 4000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('foto_carousel')) {
            $error = array('error' => $this->upload->display_errors());
            $errorstr = implode(" ", $error);

            $this->session->set_flashdata('warning', $errorstr);
            redirect(base_url('index.php/administrator/carousel/add'));
        } else {
            $data = $this->upload->data();

            $input = $this->input->post();

            $foto['title'] = $input['title'];
            $foto['description'] = $input['description'];
            $foto['foto_carousel'] = $data['file_name'];
            $foto['status'] = "Belum Disetujui";

            $this->db->insert('carousel', $foto);

            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('status', 'Data Berhasil Disimpan');
                redirect(base_url('index.php/administrator/carousel/add'));
            } else {
                $this->session->set_flashdata('warning', 'Data Gagal Disimpan');
                redirect(base_url('index.php/administrator/carousel/add'));
            }
        }
    }

    public function add_save()
    {
        $data = array(
            'title' => $this->input->post('title'),
            'description' => $this->input->post('description'),
            'foto_carousel' => $this->input->post('foto_carousel'),
        );

        $simpan = $this->db->insert('carousel', $data);

        if ($simpan) {
            redirect('administrator/carousel');
        } else {
            echo "Data Belum Masuk";
        }
    }

    public function edit($id)
    {
        $this->data['carousel'] = $this->m_carousel->edit($id);
        $this->data['title'] = 'Edit Carousel';
        $this->template->load('carousel/edit', $this->data);
    }

    public function edit_save()
    {
        $id = $this->input->post('id');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');

        if ($this->form_validation->run()) {
            $old_filename = $this->input->post('old_foto_carousel');
            $new_filename = $_FILES['foto_carousel']['name'];

            if ($new_filename == TRUE) {
                $update_filename = time() . "-" . str_replace(" ", "-", $_FILES['foto_carousel']['name']);

                $config['upload_path']          = './uploads/carousel/';
                $config['allowed_types']        = 'jpeg|jpg|png';
                $config['max_size']             = 4000;
                $config['max_width']            = 6000;
                $config['max_height']           = 4000;
                $config['file_name']            = $update_filename;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('foto_carousel')) {
                    if (file_exists("./uploads/carousel/" . $old_filename)) {
                        unlink("./uploads/carousel/" . $old_filename);
                    }
                }
            } else 
            {
                $update_filename = $old_filename;
            }
            $input = $this->input->post();

            $data['title'] = $input['title'];
            $data['description'] = $input['description'];
            $data['foto_carousel'] = $update_filename;
            $data['status'] = $input['status'];

            $update = new M_carousel;
            $result = $update->edit_save($data,$id);
            
            $this->session->set_flashdata('status', 'Data Berhasil Diupdate');
                redirect(base_url('index.php/administrator/carousel/edit/'.$id));
        } else {
            $this->session->set_flashdata('warning', 'Data Gagal Diupdate');
                redirect(base_url('index.php/administrator/carousel/edit/'.$id));
        }
    }

    public function hapus($id)
    {
        $pro = new M_carousel;
        if($pro->checkImage($id)){
            $data = $pro->checkImage($id);
            if(file_exists("./uploads/carousel/" . $data->foto_carousel)){
                unlink("./uploads/carousel/" . $data->foto_carousel);
            }
            $pro->deletePro($id);
            $this->session->set_flashdata('status', 'Data Berhasil Dihapus');
            redirect(base_url('index.php/administrator/carousel'));
        }
    }
}
