<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        if(!isset($this->session->userdata['username'])){
            redirect(base_url());
        }
    }

    public function index(){
        $this->data['title'] = 'Beranda';

        $this->template->load('dashboard', $this->data);
    }

    public function destroy(){
        $this->session->sess_destroy();
        redirect('login');
    }
}
?>