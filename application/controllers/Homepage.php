<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Homepage extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_carousel');
		$this->load->model('m_product');
	}

	public function index()
	{

		$query = $this->m_carousel->getacccarousel();
		$count = count($query);
		$indicators = '';
		$slides = '';
		$counter = 0;
		foreach ($query as $key => $value) {
			$image = $query[$key]['foto_carousel'];
			$title = $query[$key]['title'];
			$description = $query[$key]['description'];
			if ($counter == 0) {
				$indicators .= '<li data-target="#carouselExampleCaptions" data-slide-to="' . $counter . '" 
            class="active"></li>';
				$slides .= '<div class="carousel-item active">
            <img src="'.base_url('uploads/carousel/'.$image).'" alt="' . $title . '" class="d-block w-100 h-auto"/>
            <div class="carousel-caption d-none d-md-block caro-capt">
                  <h5>' .$title. '</h5>
                  <p>' .$description. '</p>
               </div>
            </div>';
			} else {
				$indicators .= '<li data-target="#carouselExampleCaptions" data-slide-to="' . $counter . '" 
            class=""></li>';
				$slides .= '<div class="carousel-item ">
            <img src="'.base_url('uploads/carousel/'.$image).'" alt="' . $title . '" class="d-block w-100 h-auto"/>
            <div class="carousel-caption d-none d-md-block caro-capt">
                  <h5>' .$title. '</h5>
                  <p>' .$description. '</p>
               </div>
            </div>';
			}
			$counter = $counter + 1;
		}
		$data['indicators'] = $indicators;
		$data['slides'] = $slides;
		$this->load->view('homepage', $data);
	}

	public function product()
	{
		$this->data['product'] = $this->m_product->getaccproduct();
        $this->load->view('product', $this->data);
	}

	public function login()
	{
		$this->load->view('login');
	}

	public function filter(){

		$nama_barang = $this->input->post('nama_barang');
		$jenis_barang = $this->input->post('jenis_barang');
		$harga_maksimal = $this->input->post('harga_maksimal');
		$harga_minimal = $this->input->post('harga_minimal');
		$this->data['product'] = $this->m_product->filtering($nama_barang,$jenis_barang,$harga_minimal,$harga_maksimal);
        $this->load->view('product', $this->data);
		}
}
