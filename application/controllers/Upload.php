<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Upload extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function index()
        {
                $this->load->view('carousel/add');
        }

    public function do_upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 1000;
        $config['max_width']            = 2048;
        $config['max_height']           = 1080;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('foto_carousel')) {
            $error = array('error' => $this->upload->display_errors());

            echo "Upload Gagal";
        } else {
            $data = $this->upload->data();

            $input = $this->input->post();

            $foto['title'] = $input['title'];
            $foto['description'] = $input['description'];
            $foto['foto_carousel'] = $input['file_name'];
            $foto['status'] = 'Belum Disetujui';

            $this->db->insert('carousel', $foto);

            if($this->db->affected_rows()>0){
                echo "Data Berhasil Disimpan";
            }else{
                echo "Data Gagal Disimpan";
            }
        }
    }
}
